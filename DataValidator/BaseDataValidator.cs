﻿namespace DataValidator
{
    using System;
    using System.Collections.Generic;
    using AutoTradingDataService.Entities;
    using AutoTradingDataService.Utils;
    using log4net;

    public abstract class BaseDataValidator
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(BaseDataValidator));

        protected string BaseLocation { get; }
        public string Pair { get; }
        protected int MinBreakSizeMs { get; }
        public List<DateTime> MissingDays { get; }
        public Dictionary<DateTime, List<Tuple<long, TickData>>> Breaks { get; }
        public Dictionary<DateTime, List<TickData>> InvalidTicks { get; }

        protected BaseDataValidator(string baseLocation, string pair, int minBreakSizeMS = 0)
        {
            this.BaseLocation = baseLocation;
            this.Pair = pair;
            this.MinBreakSizeMs = minBreakSizeMS > 0 ? minBreakSizeMS : 10 * 60 * 1000;
            
            this.MissingDays = new List<DateTime>();
            this.Breaks = new Dictionary<DateTime, List<Tuple<long, TickData>>>();
            this.InvalidTicks = new Dictionary<DateTime, List<TickData>>();
        }

        public void ValidateDateRange(DateTime startDate, DateTime endDate)
        {
            for (var currentDate = startDate; currentDate < endDate; currentDate = currentDate.AddDays(1))
            {
                if (currentDate.DayOfWeek == DayOfWeek.Saturday || currentDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }

                this.ValidateDate(currentDate.Date);
            }
        }

        protected void ValidateTickData(DateTime date, IEnumerable<TickData> tickDataSet)
        {
            long currentTimestamp = DateUtils.GetTimestampFromDateTime(date);
            foreach (var tickData in tickDataSet)
            {
                if (!tickData.IsValid() || tickData.TickTime < currentTimestamp)
                {
                    if (!this.InvalidTicks.ContainsKey(date)) this.InvalidTicks.Add(date, new List<TickData>());
                    this.InvalidTicks[date].Add(tickData);
                }

                if (tickData.TickTime - currentTimestamp >= this.MinBreakSizeMs)
                {
                    if (!this.Breaks.ContainsKey(date)) this.Breaks.Add(date, new List<Tuple<long, TickData>>());
                    this.Breaks[date].Add(new Tuple<long, TickData>(currentTimestamp, tickData));
                }

                currentTimestamp = tickData.TickTime;
            }
        }

        protected abstract void ValidateDate(DateTime date);
    }
}