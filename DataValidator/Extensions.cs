﻿namespace DataValidator
{
    using System;
    using AutoTradingDataService.Utils;
    using log4net;

    public static class Extensions
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Extensions));

        private static void OutputResults(this BaseDataValidator dataValidator)
        {
            foreach (var missingDay in dataValidator.MissingDays)
            {
                Log.InfoFormat("{0} - Missing date: {1:dd/MM/yyyy}", dataValidator.Pair, missingDay);
            }

            foreach (var hour in dataValidator.InvalidTicks)
            {
                Log.InfoFormat("{0} - Found invalid ticks for hour: {1:dd/MM/yyyy HH}h:", dataValidator.Pair, hour.Key);
                foreach (var invalidTick in hour.Value)
                {
                    Log.InfoFormat("Invalid tick: {0}", invalidTick);
                }
            }

            foreach (var hour in dataValidator.Breaks)
            {
                Log.InfoFormat("{0} - Found breaks for hour: {1:dd/MM/yyyy HH}h:", dataValidator.Pair, hour.Key);
                foreach (var tickBreak in hour.Value)
                {
                    Log.InfoFormat("Tick Break: {0} ({1:dd/MM/yyyy HH:mm:ss.zzz}) - {2} ({3:dd/MM/yyyy HH:mm:ss.zzz}) (total: {4:N2} s) for tick: {5}",
                        tickBreak.Item2.TickTime, DateUtils.GetDateTimeFromTimestamp(tickBreak.Item2.TickTime),
                        tickBreak.Item1, DateUtils.GetDateTimeFromTimestamp(tickBreak.Item1),
                        (tickBreak.Item2.TickTime - tickBreak.Item1) / 1000.0m,
                        tickBreak.Item2);
                }
            }
        }

        public static void OutputResults(this RawDataValidator dataValidator)
        {
            ((BaseDataValidator) dataValidator).OutputResults();

            foreach (var incompleteDay in dataValidator.MissingHours)
            {
                Log.InfoFormat("{0} - Incomplete date: {1:dd/MM/yyyy}:", dataValidator.Pair, incompleteDay.Key);
                foreach (var missingHour in incompleteDay.Value)
                {
                    Log.InfoFormat("{0} - Missing hour: {1:dd/MM/yyyy HH}h", dataValidator.Pair, missingHour);
                }
            }

            foreach (var incompleteDay in dataValidator.UnreadableHours)
            {
                Log.InfoFormat("{0} - Incomplete date: {1:dd/MM/yyyy}:", dataValidator.Pair, incompleteDay.Key);
                foreach (var unreadableHour in incompleteDay.Value)
                {
                    Log.InfoFormat("{0} - Unreadable hour: {1:dd/MM/yyyy HH}h", dataValidator.Pair, unreadableHour);
                }
            }
        }

        public static void OutputResults(this CsvDataValidator dataValidator)
        {
            ((BaseDataValidator) dataValidator).OutputResults();

            foreach (var unreadableDay in dataValidator.UnreadableDays)
            {
                Log.InfoFormat("{0} - Missing date: {1:dd/MM/yyyy}", dataValidator.Pair, unreadableDay);
            }
        }
    }
}