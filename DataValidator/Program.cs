﻿namespace DataValidator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoTradingDataService.Entities;
    using AutoTradingDataService.Utils;
    using log4net;

    internal class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        private const string kDukascopyRawPath = "V:\\DATA\\DukasCopyRaw";
        private const string kDukascopyCsvPath = "V:\\DATA\\DukasCopy";
        
        public static void Main(string[] args)
        {
            var startDate = new DateTime(2016, 1, 1);
            var pairs = GetPairs();
            var lastDownloadPerPair = PathUtils.GetLastPathForAllPairs(kDukascopyRawPath, pairs.Keys, startDate, true);
            var lastCsvPerPair = PathUtils.GetLastPathForAllPairs(kDukascopyCsvPath, pairs.Keys, startDate);

            foreach (string pair in pairs.Keys)
            {
                var lastDownload = lastDownloadPerPair[pair];
                double downloadProgress = (lastDownload - startDate).TotalDays / (DateTime.Now - startDate).TotalDays;
                var lastCsv = lastCsvPerPair[pair];
                double csvProgress = (lastCsv - startDate).TotalDays / (DateTime.Now - startDate).TotalDays;
                Log.InfoFormat("Checking pair: {0}", pair);
                Log.InfoFormat("{0} - last download: {1:dd/MM/yyyy HH}h, progress: {2:P}", pair, lastDownload, downloadProgress);
                Log.InfoFormat("{0} - last CSV: {1:dd/MM/yyyy}, progress: {2:P}", pair, lastCsv, csvProgress);
                
                var rawValidator = new RawDataValidator(kDukascopyRawPath, pair, pairs[pair]);
                Log.InfoFormat("{0} - raw files validation", pair);
                rawValidator.ValidateDateRange(startDate, lastDownload.Date);
                rawValidator.OutputResults();
                
                var csvValidator = new CsvDataValidator(kDukascopyCsvPath, pair);
                Log.InfoFormat("{0} - CSV files validation", pair);
                csvValidator.ValidateDateRange(startDate, lastCsv.Date);
                csvValidator.OutputResults();
            }
            
            Log.Info("Job's done!!");
        }
 
        private static Dictionary<string, int> GetPairs()
        {
            using (var db = new DukascopyContext())
            {
                return db.Pairs.ToDictionary(x => x.Name, x => x.Points);
            }
        }
    }
}