﻿namespace DataValidator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using AutoTradingDataService.Entities;
    using AutoTradingDataService.Utils;

    public class CsvDataValidator : BaseDataValidator
    {
        public List<DateTime> UnreadableDays { get; }

        public CsvDataValidator(string baseLocation, string pair, int minBreakSizeMS = 0)
            : base(baseLocation, pair, minBreakSizeMS)
        {
            this.UnreadableDays = new List<DateTime>();
        }

        protected override void ValidateDate(DateTime date)
        {
            string sourceFile = PathUtils.GetCsvFilePath(this.BaseLocation, this.Pair, date);
            if (!File.Exists(sourceFile))
            {
                this.MissingDays.Add(date);
                return;
            }
            
            TickData[] tickDataSet;
            try
            {
                tickDataSet = CsvUtils.ReadTickData(this.Pair, sourceFile).ToArray();
            }
            catch (Exception ex)
            {
                if (!this.UnreadableDays.Contains(date)) this.UnreadableDays.Add(date);
                this.UnreadableDays.Add(date);
                Log.Error("Error while processing CSV file", ex);
                return;
            }
            
            base.ValidateTickData(DateUtils.GetDayStart(date), tickDataSet);
        }
    }
}