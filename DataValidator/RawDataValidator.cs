﻿namespace DataValidator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using AutoTradingDataService.Entities;
    using AutoTradingDataService.Utils;

    public class RawDataValidator : BaseDataValidator
    {
        private readonly int _pairPoints;
        public Dictionary<DateTime, List<DateTime>> MissingHours { get; }
        public Dictionary<DateTime, List<DateTime>> UnreadableHours { get; }

        public RawDataValidator(string baseLocation, string pair, int pairPoints, int minBreakSizeMS = 0)
            : base(baseLocation, pair, minBreakSizeMS)
        {
            this._pairPoints = pairPoints;
            this.MissingHours = new Dictionary<DateTime, List<DateTime>>();
            this.UnreadableHours = new Dictionary<DateTime, List<DateTime>>();
        }

        protected override void ValidateDate(DateTime date)
        {
            string sourceDir = PathUtils.GetPath(this.BaseLocation, this.Pair, date, true);
            if (!Directory.Exists(sourceDir))
            {
                this.MissingDays.Add(date);
                return;
            }

            foreach (var hour in DateUtils.EnumerateHours(date))
            {
                this.ValidateHour(date, hour);
            }

            if (this.MissingHours.ContainsKey(date) && this.MissingHours[date].Count == 24)
            {
                if (!this.MissingDays.Contains(date)) this.MissingDays.Add(date);
            }
        }

        private void ValidateHour(DateTime date, DateTime hour)
        {
            string sourceFile = PathUtils.GetRawFilePath(this.BaseLocation, this.Pair, hour);
            if (!File.Exists(sourceFile))
            {
                if (!this.MissingHours.ContainsKey(date)) this.MissingHours.Add(date, new List<DateTime>());
                this.MissingHours[date].Add(hour);
                return;
            }

            long hourTimestamp = DateUtils.GetTimestampFromDateTime(hour);
            TickData[] tickDataSet;
            try
            {
                tickDataSet = DukascopyUtils.ReadFile(this.Pair, sourceFile, hourTimestamp, this._pairPoints).ToArray();
            }
            catch (Exception ex)
            {
                if (!this.UnreadableHours.ContainsKey(date)) this.UnreadableHours.Add(date, new List<DateTime>());
                this.UnreadableHours[date].Add(hour);
                Log.Error("Error while processing Raw file", ex);
                return;
            }

            base.ValidateTickData(hour, tickDataSet);
        }
    }
}