﻿namespace AutoTradingDataService
{
    using System.Timers;

    abstract class BaseWorker
    {
        private Timer _timer;

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Execute();
        }

        protected abstract void Execute();

        internal void OnPause()
        {
            this._timer.Stop();
        }

        internal void OnContinue()
        {
            this._timer.Start();
        }

        internal void Kill()
        {
            this._timer.Stop();
            this._timer = null;
        }

        protected void SetAndStartTimer(double waitSeconds)
        {
            if (this._timer == null)
            {
                this._timer = new Timer();
                this._timer.Elapsed += this.timer_Elapsed;
            }

            this._timer.Interval = waitSeconds * 1000;
            this._timer.Start();
        }

        protected void StopTimer()
        {
            this._timer.Stop();
        }
    }
}
