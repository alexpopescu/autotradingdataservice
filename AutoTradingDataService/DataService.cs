﻿namespace AutoTradingDataService
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.ServiceProcess;
    using log4net;

    public class DataService : ServiceBase, IDebuggableService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataService));

        private List<BaseWorker> _workers;

        ~DataService()
        {
            this.KillWorkers();
        }

        protected override void OnShutdown()
        {
            this.KillWorkers();
            base.OnShutdown();
        }

        protected override void OnStart(string[] args)
        {
            if (this._workers != null && this._workers.Any()) this.OnStop();

            Log.Info("Starting service...");
            if (this._workers == null)
            {
                var config = ConfigurationManager.GetSection("dukascopyDownloader") as Config.DukascopySection;
                this._workers = new List<BaseWorker>
                {
                    new DukascopyDownloadWorker(config),
                    new DukascopyConvertorWorker(config),
                    new ForexAggregatorWorker(config)
                };
            }
            base.OnStart(args);
            Log.Info("Service started");
        }

        protected override void OnContinue()
        {
            Log.Info("Resuming execution...");
            if (this._workers != null) this._workers.ForEach(x => x.OnContinue());
            base.OnContinue();
            Log.Info("Execution resumed");
        }

        protected override void OnPause()
        {
            Log.Info("Pausing execution...");
            if (this._workers != null) this._workers.ForEach(x => x.OnPause());
            base.OnPause();
            Log.Info("Execution paused");
        }

        protected override void OnStop()
        {
            Log.Info("Stopping service...");
            this.KillWorkers();
            base.OnStop();
            Log.Info("Service stopped");
        }

        private void KillWorkers()
        {
            if (this._workers != null)
            {
                this._workers.ForEach(x => x.Kill());
                this._workers = null;
            }
        }

        #region IDebuggableService
        void AutoTradingDataService.IDebuggableService.Start(string[] args)
        {
            this.OnStart(args);
        }

        void AutoTradingDataService.IDebuggableService.Stop()
        {
            this.OnStop();
        }

        void AutoTradingDataService.IDebuggableService.Pause()
        {
            this.OnPause();
        }

        void AutoTradingDataService.IDebuggableService.Continue()
        {
            this.OnContinue();
        }
        #endregion

    }
}
