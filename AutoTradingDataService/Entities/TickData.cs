﻿namespace AutoTradingDataService.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("dukascopy.data")]
    public class TickData
    {
        [Key]
        [Required]
        [Column("fk_pair", Order = 0)]
        public string Pair { get; set; }

        [Key]
        [Required]
        [Column("ticktime", Order = 1)]
        public long TickTime { get; set; }

        [Required]
        [Column("bid_price")]
        public decimal BidPrice { get; set; }

        [Required]
        [Column("ask_price")]
        public decimal AskPrice { get; set; }

        [Required]
        [Column("bid_volume")]
        public decimal BidVolume { get; set; }

        [Required]
        [Column("ask_volume")]
        public decimal AskVolume { get; set; }

        public bool IsValid()
        {
            return this.AskPrice > this.BidPrice
                   && this.AskPrice > 0 && this.BidPrice > 0
                   && this.AskVolume >= 0 && this.BidVolume >= 0
                   && (this.AskPrice - this.BidPrice <= this.AskPrice * 0.1m);
        }

        public override string ToString()
        {
            return string.Format(@"[{0}] {1}: bid {2} @ {3}, ask {4} @ {5}",
                this.Pair, this.TickTime, this.BidVolume, this.BidPrice, this.AskVolume, this.AskPrice);
        }
    }
}
