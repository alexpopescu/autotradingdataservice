﻿namespace AutoTradingDataService.Entities
{
    using System.Data.Entity;

    public class DukascopyContext : DbContext
    {
        public DukascopyContext()
            : base("Name=DukasCopy")
        {
        }

        public virtual DbSet<Pair> Pairs { get; set; }
        public virtual DbSet<TickData> TickData { get; set; }
        public virtual DbSet<CandleStick> CandleSticks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pair>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<TickData>()
                .Property(e => e.Pair)
                .IsUnicode(false);

            modelBuilder.Entity<TickData>()
                .Property(e => e.AskPrice)
                .HasPrecision(10, 6);

            modelBuilder.Entity<TickData>()
                .Property(e => e.BidPrice)
                .HasPrecision(10, 6);

            modelBuilder.Entity<TickData>()
                .Property(e => e.AskVolume)
                .HasPrecision(6, 2);

            modelBuilder.Entity<TickData>()
                .Property(e => e.BidVolume)
                .HasPrecision(6, 2);

            modelBuilder.Entity<CandleStick>()
                .Property(e => e.Open)
                .HasPrecision(10, 6);

            modelBuilder.Entity<CandleStick>()
                .Property(e => e.Close)
                .HasPrecision(10, 6);

            modelBuilder.Entity<CandleStick>()
                .Property(e => e.Low)
                .HasPrecision(10, 6);

            modelBuilder.Entity<CandleStick>()
                .Property(e => e.High)
                .HasPrecision(10, 6);
        }
    }
}
