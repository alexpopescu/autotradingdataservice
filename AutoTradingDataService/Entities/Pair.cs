﻿namespace AutoTradingDataService.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("dukascopy.pair")]
    public class Pair
    {
        [Key]
        [Required]
        [Column("pk_pair")]
        public string Name { get; set; }

        [Required]
        [Column("points")]
        public int Points { get; set; }
    }
}
