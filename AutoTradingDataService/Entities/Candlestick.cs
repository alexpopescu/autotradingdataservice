﻿namespace AutoTradingDataService.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Code;

    [Table("dukascopy.candlestick")]
    public class CandleStick
    {
        private readonly Dictionary<PriceLevel, decimal> _prices = new Dictionary<PriceLevel, decimal>
        {
            { PriceLevel.Open, 0 },
            { PriceLevel.Close, 0 },
            { PriceLevel.High, 0 },
            { PriceLevel.Low, 0 }
        };

        [Key]
        [Required]
        [Column("fk_pair", Order = 0)]
        public string Pair { get; set; }

        [Key]
        [Required]
        [Column("period", Order = 1)]
        public Period Period { get; set; }

        [Key]
        [Required]
        [Column("starttime", Order = 2)]
        public long StartTime { get; set; }

        [Required]
        [Column("volume")]
        public decimal Volume { get; set; }

        [Required]
        [Column("open")]
        public decimal Open
        {
            get => this._prices[PriceLevel.Open];
            set => this._prices[PriceLevel.Open] = value;
        }

        [Required]
        [Column("close")]
        public decimal Close
        {
            get => this._prices[PriceLevel.Close];
            set => this._prices[PriceLevel.Close] = value;
        }

        [Required]
        [Column("low")]
        public decimal Low
        {
            get => this._prices[PriceLevel.Low];
            set => this._prices[PriceLevel.Low] = value;
        }


        [Required]
        [Column("high")]
        public decimal High
        {
            get => this._prices[PriceLevel.High];
            set => this._prices[PriceLevel.High] = value;
        }

        public decimal GetByLevel(PriceLevel level)
        {
            return this._prices[level];
        }

        public void Tick(decimal price, decimal volume)
        {
            this.Volume += volume;
            if (this.Open == 0) this.Open = price;
            if (this.High == 0 || this.High < price) this.High = price;
            if (this.Low == 0 || this.Low > price) this.Low = price;
            this.Close = price;
        }
    }
}
