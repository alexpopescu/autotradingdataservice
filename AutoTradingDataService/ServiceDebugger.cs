﻿namespace AutoTradingDataService
{
    using System;
    using System.Windows.Forms;
    using System.ServiceProcess;

    public partial class ServiceDebugger : Form
    {

        private readonly IDebuggableService idebuggableservice;

        public ServiceDebugger(IDebuggableService service)
        {
            this.InitializeComponent();
            this.idebuggableservice = service;
            this.Load += delegate
            {
                var winService = this.idebuggableservice as ServiceBase;
                if (winService != null) this.Text = winService.ServiceName + @" Controller";
            };
            this.Show();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.idebuggableservice.Start(new string[] { });
            this.lblStatus.Text = @"Started";
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            this.idebuggableservice.Pause();
            this.lblStatus.Text = @"Paused";
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            this.idebuggableservice.Continue();
            this.lblStatus.Text = @"Started";
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.idebuggableservice.Stop();
            this.lblStatus.Text = @"Stopped";
        }

    }
}
