﻿namespace AutoTradingDataService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using log4net;

    internal class DukascopyConvertorWorker : BaseWorker
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DukascopyConvertorWorker));

        private readonly bool _saveData;
        private readonly int _period;
        private readonly string _saveLocation;
        private readonly string _csvLocation;
        private readonly Dictionary<string, int> _pairs;
        private readonly Dictionary<string, DateTime> _lastUpdatePerPair;

        public DukascopyConvertorWorker(Config.DukascopySection config)
        {
            using (var db = new Entities.DukascopyContext())
            {
                this._pairs = db.Pairs.ToDictionary(x => x.Name, x => x.Points);
            }

            this._saveData = config.SaveData;
            this._saveLocation = config.SaveLocation;
            this._csvLocation = config.CsvLocation;
#if DEBUG
            this._period = 1;
#else
            this._period = 1000;
#endif

            this._lastUpdatePerPair = Utils.PathUtils.GetLastPathForAllPairs(this._csvLocation, this._pairs.Keys, config.CollectionStart);
            foreach (var lastUpdate in this._lastUpdatePerPair)
            {
                Log.InfoFormat("Last processed data for {0} was {1:dd/MM/yyyy HH}h",
                    lastUpdate.Key, lastUpdate.Value);
            }

            Log.Info("Starting execution");
            Task.Factory.StartNew(this.SetTimer);
        }

        protected override void Execute()
        {
            base.StopTimer();

            try
            {
                string pair = this._lastUpdatePerPair.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
                var nextUpdate = this._lastUpdatePerPair[pair];
                var lastDownloadForPair = Utils.PathUtils.GetLastPathForPair(this._saveLocation, pair, isHourly: true);
                bool isComplete = false;
                int daylightSavingDiff = 0;

                while (nextUpdate < lastDownloadForPair)
                {
                    nextUpdate = nextUpdate.AddDays(1);
                    string sourceDir = Utils.PathUtils.GetPath(
                        this._saveLocation, pair, nextUpdate, true);

                    if (!Directory.Exists(sourceDir)) continue;

                    int hourFilesCount = Directory.EnumerateFiles(sourceDir).Count();
                    daylightSavingDiff = Utils.DateUtils.IsDaylightSavingTime(nextUpdate) ? 1 : 0;

                    isComplete = (nextUpdate.DayOfWeek == DayOfWeek.Friday && hourFilesCount == 22 - daylightSavingDiff)
                                 || hourFilesCount == 24;
                    if (isComplete) break;

                    if (nextUpdate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        Log.ErrorFormat("Incomplete day for {0}: {1:dd/MM/yyyy}", pair, nextUpdate);
                    }
                }

                if (isComplete && this._saveData)
                {
                    Log.InfoFormat("Converting data for pair {0} date {1:dd/MM/yyyy}", pair, nextUpdate);
                    string destinationFile = Utils.PathUtils.GetCsvFilePath(
                        this._csvLocation, pair, nextUpdate);
                    var destFileInfo = new FileInfo(destinationFile);

                    if (destFileInfo.DirectoryName != null && !Directory.Exists(destFileInfo.DirectoryName))
                    {
                        Log.InfoFormat("Creating directory {0}", destFileInfo.DirectoryName);
                        Directory.CreateDirectory(destFileInfo.DirectoryName);
                    }

                    this._lastUpdatePerPair[pair] = nextUpdate;
                    int startHour = -2 - daylightSavingDiff;
                    int endHour = 21 - daylightSavingDiff;

                    for (int hour = startHour; hour <= endHour; hour++)
                    {
                        string sourceFile = Utils.PathUtils.GetRawFilePath(this._saveLocation, pair, nextUpdate);
                        if (!File.Exists(sourceFile))
                        {
                            Log.ErrorFormat("File not found: {0}", sourceFile);
                            return;
                        }

                        long hourTimestamp = Utils.DateUtils.GetTimestampFromDateTime(nextUpdate.AddHours(hour));
                        var tickDataSet = Utils.DukascopyUtils.ReadFile(pair, sourceFile, hourTimestamp, this._pairs[pair]).ToArray();
                        Log.InfoFormat("{0} lines for {1:dd/MM/yyyy} {2:00}h", tickDataSet.Length, nextUpdate, hour);
                        File.AppendAllLines(destinationFile, tickDataSet.Select(
                            t => string.Format("{0},{1},{2},{3},{4}", t.TickTime, t.BidPrice, t.AskPrice, t.BidVolume, t.AskVolume)
                        ));
                    }
                }

                Log.Info("Done");
            }
            catch (Exception ex)
            {
                Log.Error("Error while getting data", ex);
            }

            this.SetTimer();
        }

        private void SetTimer()
        {
            int waitSeconds = this._period;
            Log.InfoFormat("The next run will be in {0} seconds", waitSeconds);
            base.SetAndStartTimer(waitSeconds);
        }
    }
}