﻿namespace AutoTradingDataService
{
    using System.ServiceProcess;
    using System.Windows.Forms;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            Application.Run(new ServiceDebugger(new DataService()));
#else
            ServiceBase[] servicesToRun = { new DataService() };
            ServiceBase.Run(servicesToRun);
#endif
        }
    }
}
