﻿namespace AutoTradingDataService.Config
{
    using System;
    using System.Configuration;

    public class DukascopySection : ConfigurationSection
    {
        [ConfigurationProperty("saveData")]
        public bool SaveData
        {
            get => (bool)this["saveData"];
            set => this["saveData"] = value;
        }

        [ConfigurationProperty("waitPeriodMin")]
        public int WaitPeriodMin
        {
            get => (int) this["waitPeriodMin"];
            set => this["waitPeriodMin"] = value;
        }

        [ConfigurationProperty("waitPeriodMax")]
        public int WaitPeriodMax
        {
            get => (int) this["waitPeriodMax"];
            set => this["waitPeriodMax"] = value;
        }

        [ConfigurationProperty("url")]
        public string Url
        {
            get => (string)this["url"];
            set => this["url"] = value;
        }

        [ConfigurationProperty("userAgent")]
        public string UserAgent
        {
            get => (string)this["userAgent"];
            set => this["userAgent"] = value;
        }

        [ConfigurationProperty("saveLocation")]
        public string SaveLocation
        {
            get => (string)this["saveLocation"];
            set => this["saveLocation"] = value;
        }

        [ConfigurationProperty("csvLocation")]
        public string CsvLocation
        {
            get => (string)this["csvLocation"];
            set => this["csvLocation"] = value;
        }

        [ConfigurationProperty("collectionStart")]
        public DateTime CollectionStart
        {
            get => (DateTime)this["collectionStart"];
            set => this["collectionStart"] = value;
        }
    }
}
