﻿namespace AutoTradingDataService
{
    using System;
    using System.IO;

    public class BinaryReaderBigEndian : BinaryReader
    {
        public BinaryReaderBigEndian(System.IO.Stream stream) : base(stream)
        {
        }

        public override int ReadInt32()
        {
            var x = base.ReadBytes(4);
            Array.Reverse(x);
            return BitConverter.ToInt32(x, 0);
        }

        public override short ReadInt16()
        {
            var x = base.ReadBytes(2);
            Array.Reverse(x);
            return BitConverter.ToInt16(x, 0);
        }

        public override long ReadInt64()
        {
            var x = base.ReadBytes(8);
            Array.Reverse(x);
            return BitConverter.ToInt64(x, 0);
        }

        public override uint ReadUInt32()
        {
            var x = base.ReadBytes(4);
            Array.Reverse(x);
            return BitConverter.ToUInt32(x, 0);
        }

    }
}
