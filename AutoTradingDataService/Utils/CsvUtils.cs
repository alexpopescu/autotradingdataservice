﻿namespace AutoTradingDataService.Utils
{
    using System.Collections.Generic;
    using Entities;
    using Microsoft.VisualBasic.FileIO;

    public static class CsvUtils
    {
        public static IEnumerable<TickData> ReadTickData(string pair, string sourceFile)
        {
            using (var parser = new TextFieldParser(sourceFile))
            {
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    var fields = parser.ReadFields();
                    if (fields == null) break;

                    var tick = new TickData
                    {
                        Pair = pair,
                        TickTime = long.Parse(fields[0]),
                        BidPrice = decimal.Parse(fields[1]),
                        AskPrice = decimal.Parse(fields[2]),
                        BidVolume = decimal.Parse(fields[3]),
                        AskVolume = decimal.Parse(fields[4])
                    };

                    yield return tick;
                }
            }
        }
    }
}