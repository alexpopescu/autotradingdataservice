﻿namespace AutoTradingDataService.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Entities;

    public static class DukascopyUtils
    {
        public static IEnumerable<TickData> ReadFile(string pair, string sourceFile, long hourTimestamp, int pairPoints)
        {
            return ParseData(pair, File.ReadAllBytes(sourceFile), hourTimestamp, pairPoints);
        }

        public static IEnumerable<Entities.TickData> ParseData(string pair, byte[] data, long hourTimestamp, int pairPoints)
        {
            using (Stream inStream = new MemoryStream(data))
            using (Stream outStream = new MemoryStream())
            {
                SevenZip.Helper.Decompress(inStream, outStream);
                outStream.Position = 0;
                using (var reader = new BinaryReaderBigEndian(outStream))
                {
                    while (reader.PeekChar() != -1)
                    {
                        long tickTime = hourTimestamp + reader.ReadInt32();
                        decimal askPrice = (decimal) reader.ReadInt32() / pairPoints;
                        decimal bidPrice = (decimal) reader.ReadInt32() / pairPoints;
                        
                        var askVolumeArr = reader.ReadBytes(4);
                        Array.Reverse(askVolumeArr);
                        decimal askVolume = (decimal) BitConverter.ToSingle(askVolumeArr, 0);
                        
                        var bidVolumeArr = reader.ReadBytes(4);
                        Array.Reverse(bidVolumeArr);
                        decimal bidVolume = (decimal) BitConverter.ToSingle(bidVolumeArr, 0);
                        
                        yield return new TickData()
                        {
                            Pair = pair,
                            TickTime = tickTime,
                            BidPrice = bidPrice,
                            AskPrice = askPrice,
                            BidVolume = bidVolume,
                            AskVolume = askVolume
                        };
                    }
                }
            }
        }
    }
}