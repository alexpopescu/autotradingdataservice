﻿namespace AutoTradingDataService.Utils
{
    using System;
    using System.Collections.Generic;

    public static class DateUtils
    {
        public static DateTime UnixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        public static bool IsDaylightSavingTime(DateTime date)
        {
            if (date.Month > 3 && date.Month < 11) return true;

            int dayOfWeek = (int)(new DateTime(date.Year, date.Month, 1).DayOfWeek);
            if (dayOfWeek == 0) dayOfWeek = 7; // sunday is 7
            switch (date.Month)
            {
                case 3 when date.Day > 13 - dayOfWeek:
                case 11 when date.Day < 7 - dayOfWeek:
                    return true;
                default:
                    return false;
            }
        }

        public static long GetTimestampFromDateTime(DateTime date)
        {
            return (long)(date - UnixEpochStart).TotalMilliseconds;
        }

        public static DateTime GetDateTimeFromTimestamp(long timestamp)
        {
            return UnixEpochStart.AddMilliseconds(timestamp);
        }

        public static IEnumerable<DateTime> EnumerateHours(DateTime date)
        {
            int daylightSavingDiff = IsDaylightSavingTime(date) ? 1 : 0;
            int startHour = -2 - daylightSavingDiff;
            int endHour = 21 - daylightSavingDiff;

            for (int hour = startHour; hour <= endHour; hour++)
            {
                yield return date.AddHours(hour);
            }
        }

        public static DateTime GetDayStart(DateTime date)
        {
            int daylightSavingDiff = IsDaylightSavingTime(date) ? 1 : 0;
            return date.AddHours(-2 - daylightSavingDiff);
        }
    }
}