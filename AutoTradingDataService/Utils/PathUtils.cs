﻿namespace AutoTradingDataService.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public static class PathUtils
    {
        public static string GetPath(string basePath, string pair, DateTime date,
            bool includeDay = false)
        {
            string path = Path.Combine(
                basePath, pair,
                date.Year.ToString(),
                date.Month.ToString("00"));

            if (includeDay)
            {
                path = Path.Combine(path, date.Day.ToString("00"));
            }

            return path;
        }

        public static string GetCsvFilePath(string basePath, string pair, DateTime date)
        {
            return Path.Combine(
                GetPath(basePath, pair, date),
                string.Format("{0:00}.csv", date.Day));
        }

        public static string GetRawFilePath(string basePath, string pair, DateTime date)
        {
            return Path.Combine(
                GetPath(basePath, pair, date, true),
                string.Format("{0:00}h_ticks.bi5", date.Hour));
        }

        public static Dictionary<string, DateTime> GetLastPathForAllPairs(string baseDirectory, IEnumerable<string> pairs, DateTime startDate, bool isHourly = false)
        {
            var lastUpdatePerPair = new Dictionary<string, DateTime>();
            foreach (string pair in pairs)
            {
                var lastUpdate = GetLastPathForPair(baseDirectory, pair, startDate, isHourly);
                lastUpdatePerPair.Add(pair, lastUpdate);
            }

            return lastUpdatePerPair;
        }

        public static DateTime GetLastPathForPair(string baseDirectory, string pair, DateTime? startDate = null, bool isHourly = false)
        {
            var lastUpdate = startDate ?? DateTime.MinValue;
            string currPairDir = Path.Combine(baseDirectory, pair);
            if (!Directory.Exists(currPairDir)) return lastUpdate;

            string yearDir = GetLastDirectory(currPairDir);
            if (yearDir == null) return lastUpdate;

            int year = int.Parse(new DirectoryInfo(yearDir).Name);
            string monthDir = GetLastDirectory(yearDir);
            if (monthDir == null) return lastUpdate;

            int month = int.Parse(new DirectoryInfo(monthDir).Name);
            if (isHourly)
            {
                string dayDir = GetLastDirectory(monthDir);
                if (dayDir == null) return lastUpdate;

                int day = int.Parse(new DirectoryInfo(dayDir).Name);
                string hourFile = GetLastFile(dayDir);
                if (hourFile == null) return lastUpdate;

                int hour = int.Parse(new FileInfo(hourFile).Name.Substring(0, 2));
                return new DateTime(year, month, day, hour, 0, 0);
            }
            else
            {
                string dayFile = GetLastFile(monthDir);
                if (dayFile == null) return lastUpdate;

                int day = int.Parse(new FileInfo(dayFile).Name.Substring(0, 2));
                return new DateTime(year, month, day);
            }
        }

        public static string GetLastDirectory(string baseDirectory)
        {
            return Directory.EnumerateDirectories(baseDirectory).OrderByDescending(name => name).FirstOrDefault();
        }

        public static string GetLastFile(string baseDirectory)
        {
            return Directory.EnumerateFiles(baseDirectory).OrderByDescending(name => name).FirstOrDefault();
        }
    }
}