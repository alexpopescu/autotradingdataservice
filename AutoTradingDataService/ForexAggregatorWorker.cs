﻿namespace AutoTradingDataService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Code;
    using Entities;
    using log4net;
    using Microsoft.VisualBasic.FileIO;
    using Utils;

    internal class ForexAggregatorWorker : BaseWorker
    {
        private class NextRun
        {
            public string Pair { get; }
            public DateTime Date { get; }

            public NextRun(string pair, DateTime date)
            {
                this.Pair = pair;
                this.Date = date;
            }
        }
        
        private static readonly ILog Log = LogManager.GetLogger(typeof(ForexAggregatorWorker));

        private readonly DateTime _unixEpochStart;

        private readonly bool _saveData;
        private readonly DateTime _collectionStart;
        private readonly string _csvLocation;
        private readonly int _period;

        private readonly Period[] _periods;

        public ForexAggregatorWorker(Config.DukascopySection config)
        {
            this._unixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            this._periods = Enum.GetValues(typeof(Period)).Cast<Period>().ToArray();
            this._csvLocation = config.CsvLocation;

            this._saveData = config.SaveData;
            this._collectionStart = config.CollectionStart;
#if DEBUG
            this._period = 5;
#else
            this._period = 100;
#endif
            Log.Info("Starting execution");
            Task.Factory.StartNew(this.SetTimer);
        }

        private NextRun GetNextRun(DateTime collectionStartDateTime)
        {
            using (var db = new DukascopyContext())
            {
                var lastUpdatePerPair = new Dictionary<string, DateTime>();
                foreach (string pair in db.Pairs.Select(x => x.Name))
                {
                    lastUpdatePerPair.Add(pair, collectionStartDateTime);
                }

                var lastUpdates = db.CandleSticks
                    .Where(x => x.Period == Period.D1)
                    .GroupBy(t => t.Pair)
                    .Select(t => new
                    {
                        PairName = t.Key,
                        StartTime = t.Max(x => (long?) x.StartTime)
                    });

                foreach (var entry in lastUpdates)
                {
                    if (!entry.StartTime.HasValue) continue;
                    lastUpdatePerPair[entry.PairName] = this._unixEpochStart.AddMilliseconds(entry.StartTime.Value);
                }

                var pairToUpdate = lastUpdatePerPair.Aggregate((l, r) => l.Value < r.Value ? l : r);
                return new NextRun(pairToUpdate.Key, pairToUpdate.Value.AddDays(2).Date);
            }
        }

        protected override void Execute()
        {
            base.StopTimer();
            Log.Info("Starting Aggregation");

            try
            {
                var run = this.GetNextRun(this._collectionStart);
                var nextUpdate = run.Date;
                var lastProcessedDayForPair = Utils.PathUtils.GetLastPathForPair(this._csvLocation, run.Pair);
                string sourceFile = null;

                while (nextUpdate <= lastProcessedDayForPair)
                {
                    sourceFile = Utils.PathUtils.GetCsvFilePath(this._csvLocation, run.Pair, nextUpdate);

                    if (File.Exists(sourceFile)) break;
                    nextUpdate = nextUpdate.AddDays(1);
                }

                if (nextUpdate > lastProcessedDayForPair)
                {
                    // delay one day if no days to process
                    this.SetTimer(24 * 60 * 60);
                    return;
                }

                int daylightSavingDiff = Utils.DateUtils.IsDaylightSavingTime(nextUpdate) ? 1 : 0;
                var startDate = nextUpdate.AddHours(-2 - daylightSavingDiff)
                    .AddMinutes(-nextUpdate.Minute)
                    .AddSeconds(-nextUpdate.Second)
                    .AddMilliseconds(-nextUpdate.Millisecond);
                long startTime = (long)(startDate - this._unixEpochStart).TotalMilliseconds;
                long endTime = startTime + 24 * 60 * 60 * 1000;

                if (sourceFile != null && File.Exists(sourceFile))
                {
                    Dictionary<Period, Dictionary<long, CandleStick>> result = null;

                    try
                    {
                        result = ComputeCandleSticks(this._periods, startTime, endTime, run.Pair, sourceFile);
                    }
                    catch (IOException ex)
                    {
                        Log.ErrorFormat(ex.Message);
                    }

                    using (var db = new DukascopyContext())
                    {
                        int entries = 0;
                        foreach (var kvpCandleStick in result.SelectMany(kvpPeriod => kvpPeriod.Value))
                        {
                            db.CandleSticks.Add(kvpCandleStick.Value);
                            entries++;
                        }

                        Log.InfoFormat("{0} data for pair {1} and date {2:dd/MM/yyyy}. (Total {3:N0} records)",
                            this._saveData ? "Saving" : "Simulating", run.Pair, startDate, entries);
                        if (this._saveData) db.SaveChanges();
                        Log.Info("Done");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error while getting data", ex);
            }

            this.SetTimer();
        }

        private static Dictionary<Period, Dictionary<long, CandleStick>> ComputeCandleSticks(
            Period[] periods, long startTime, long endTime, string pair, string sourceFile)
        {
            var result = new Dictionary<Period, Dictionary<long, CandleStick>>();
            foreach (var period in periods)
            {
                result.Add(period, new Dictionary<long, CandleStick>());
                long intPeriod = (long) period * 60 * 1000;
                for (long current = startTime; current < endTime; current += intPeriod)
                    result[period]
                        .Add(current, new CandleStick
                        {
                            StartTime = current,
                            Pair = pair,
                            Period = period
                        });
            }

            foreach (var tick in CsvUtils.ReadTickData(pair, sourceFile))
            {
                foreach (var period in periods)
                {
                    long numPeriod = (long) period * 60 * 1000; // period is in minutes
                    long periodStart = (tick.TickTime - startTime) / numPeriod * numPeriod + startTime;
                    if (!result[period].ContainsKey(periodStart))
                        throw new IOException(string.Format("Wrong period start for {0}: {1}", period, periodStart));

                    result[period][periodStart].Tick(tick.BidPrice, tick.BidVolume);
                }
            }

            return result;
        }

        private void SetTimer()
        {
            this.SetTimer(this._period);
        }

        private void SetTimer(int delay)
        {
            Log.InfoFormat("The next run will be in {0} seconds", delay);
            base.SetAndStartTimer(delay);
        }
    }
}
