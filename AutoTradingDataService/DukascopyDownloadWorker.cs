﻿namespace AutoTradingDataService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using log4net;

    internal class DukascopyDownloadWorker : BaseWorker
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DukascopyDownloadWorker));

        private readonly Random _random = new Random(DateTime.Now.Millisecond);
        private readonly WebClient _webClient;

        private readonly bool _saveData;
        private readonly string _url;
        private readonly int _periodMin;
        private readonly int _periodMax;
        private readonly string _saveLocation;
        private readonly Dictionary<string, int> _pairs;
        private readonly Dictionary<string, DateTime> _lastUpdatePerPair;

        public DukascopyDownloadWorker(Config.DukascopySection config)
        {
            using (var db = new Entities.DukascopyContext())
            {
                this._pairs = db.Pairs.ToDictionary(x => x.Name, x => x.Points);
            }

            this._saveData = config.SaveData;
            this._saveLocation = config.SaveLocation;
            this._url = config.Url;
#if DEBUG
            this._periodMin = 5;
            this._periodMax = 10;
#else
            this._periodMin = config.WaitPeriodMin;
            this._periodMax = config.WaitPeriodMax;
#endif

            if (this._webClient == null)
            {
                this._webClient = new WebClient
                {
                    Encoding = Encoding.UTF8
                };
                this._webClient.Headers.Add("user-agent", config.UserAgent);
            }

            this._lastUpdatePerPair = Utils.PathUtils.GetLastPathForAllPairs(
                this._saveLocation, this._pairs.Keys, config.CollectionStart, true);

            Log.Info("Starting execution");
            Task.Factory.StartNew(this.SetTimer);
        }


        protected override void Execute()
        {
            base.StopTimer();

            try
            {
                string pair = this._lastUpdatePerPair.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
                var lastUpdate = this._lastUpdatePerPair[pair].AddHours(1);

                byte[] data;
                string urlToGet = string.Format(this._url, pair, lastUpdate.Year, lastUpdate.Month - 1, lastUpdate.Day, lastUpdate.Hour);
                Log.InfoFormat("Getting data for pair {0} and date {1:dd/MM/yyyy} ({1:HH}h) from URL {2}", pair, lastUpdate, urlToGet);

                try
                {
                    data = this._webClient.DownloadData(urlToGet);
                }
                catch (WebException wex)
                {
                    Log.ErrorFormat("Error while retrieving {0}: {1}", urlToGet, wex.Message);
                    this.SetTimer(1800);
                    return;
                }

                Log.InfoFormat("Got {0:N0} bytes", data.Length);
                this._lastUpdatePerPair[pair] = lastUpdate;

                if (data.Length == 0)
                {
                    this.SetTimer(5, this._periodMin);
                    return;
                }

                if (this._saveData)
                {
                    string outFile = Utils.PathUtils.GetRawFilePath(this._saveLocation, pair, lastUpdate);
                    var fileInfo = new FileInfo(outFile);
                    if (fileInfo.DirectoryName != null && !Directory.Exists(fileInfo.DirectoryName))
                    {
                        Log.InfoFormat("Creating directory {0}", fileInfo.DirectoryName);
                        Directory.CreateDirectory(fileInfo.DirectoryName);
                    }

                    Log.InfoFormat("Saving {0} bytes to {1}", data.Length, outFile);
                    File.WriteAllBytes(outFile, data);
                }

                Log.Info("Done");
            }
            catch (Exception ex)
            {
                Log.Error("Error while getting data", ex);
            }

            this.SetTimer();
        }

        private void SetTimer()
        {
            this.SetTimer(this._periodMin, this._periodMax);
        }

        private void SetTimer(int min, int max)
        {
            this.SetTimer(this._random.Next(min, max));
        }

        private void SetTimer(int waitSeconds)
        {
            Log.InfoFormat("The next run will be in {0} seconds", waitSeconds);
            base.SetAndStartTimer(waitSeconds);
        }
    }
}