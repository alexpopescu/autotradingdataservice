﻿namespace AutoTradingDataService
{
    public enum PriceLevel
    {
        Open,
        Close,
        Low,
        High
    }
}
