﻿
namespace AutoTradingDataService
{
    public interface IDebuggableService
    {
        void Start(string[] args);
        void Stop();
        void Pause();
        void Continue();
    }
}
