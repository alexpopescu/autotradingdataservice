> ### This project is Work In Progress

# AutoTrading Data Service

A WPF Windows service that scrapes the web for various trading data sources.
The service will download and process the data to prepare it for the trainer to be used.
At the moment, it uses only one source of data but the intention is to add more soon.
After downloading the data, another worker will pick it up and aggregate it into candlesticks to be used by indicators.

### Technologies used

.NET, WPF, C#

