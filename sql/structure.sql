

CREATE TABLE `pair` (
  `pk_pair` char(6) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '10000',
  PRIMARY KEY (`pk_pair`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `data` (
  `fk_pair` char(6) NOT NULL,
  `ticktime` bigint(20) NOT NULL,
  `bid_price` decimal(10,6) NOT NULL,
  `ask_price` decimal(10,6) NOT NULL,
  `bid_volume` decimal(6,2) NOT NULL,
  `ask_volume` decimal(6,2) NOT NULL,
  PRIMARY KEY (`fk_pair`,`ticktime`),
  KEY `fk_pair_IDX` (`fk_pair`),
  CONSTRAINT `fk_data_pair` FOREIGN KEY (`fk_pair`) REFERENCES `pair` (`pk_pair`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
