﻿namespace Unpack
{
    using System;
    using System.IO;
    using AutoTradingDataService;
    using AutoTradingDataService.Utils;

    internal class Program
    {
        public static void Main(string[] args)
        {
            var unixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var hourToRead = new DateTime(2016, 9, 2, 0, 0, 0);
            long hourTimestamp = (long)(hourToRead - unixEpochStart).TotalMilliseconds;
            string sourceFile = PathUtils.GetRawFilePath("V:\\DATA\\DukasCopyRaw", "EURGBP", hourToRead);

            var data = File.ReadAllBytes(sourceFile);
            if (data.Length == 0)
            {
                Console.WriteLine("Empty file");
                return;
            }
            using (Stream inStream = new MemoryStream(data))
            using (Stream outStream = new MemoryStream())
            {
                SevenZip.Helper.Decompress(inStream, outStream);
                outStream.Position = 0;
                using (var reader = new BinaryReaderBigEndian(outStream))
                {
                    while (reader.PeekChar() != -1)
                    {
                        long tickTime = hourTimestamp + reader.ReadInt32();
                        decimal askPrice = reader.ReadInt32();
                        decimal bidPrice = reader.ReadInt32();
                        var askvolumeArr = reader.ReadBytes(4);
                        Array.Reverse(askvolumeArr);
                        decimal askVolume = (decimal)BitConverter.ToSingle(askvolumeArr, 0);
                        var bidvolumeArr = reader.ReadBytes(4);
                        Array.Reverse(bidvolumeArr);
                        decimal bidVolume = (decimal)BitConverter.ToSingle(bidvolumeArr, 0);
                        Console.WriteLine(@"{0}: bid={3}@{1}; ask={4}@{2}", tickTime, bidPrice, askPrice, bidVolume, askVolume);
                    }
                }
            }
        }
    }
}